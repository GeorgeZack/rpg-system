# Basic wizard spellbook
### Theme
The players are in a world of swords and sorcery, merlin, and gandalf both fit in here very well. Each spell is required by the ref to contain at least a spell conduit (wand or staff), and an incantation. When all requirements are met, there is a +2 Mana bonus to the spell. 

## Players spellbook
>**Name:** Fireball
>
>**Outcome:** A large sphere of fire is grown, and then floats towards my point of choosing, the sphere does damage to items, and people and floats at that point for 15 seconds
>
> **Mana:** 2
>
>|Componenet|Reason|
>|:---|---:|
>|Staff with a red ruby in the head|Focuses my mana and guids the fireball|
>|Arcane word: trizzle|Translates to burning eyes|
>|Black X made with charcoal on the floor|This guides the fireball to the location I want it to go, and allows it to stay floating above the X for the duration|
---

>**Name:** Open Lock
>
>**Outcome:** The targeted lock is physically unlocked, this spell does not break the lock and does not leave a trace of tempering, but it can only unlock non magical locks.
>
> **Mana:** 1
>
>|Componenet|Reason|
>|:---|---:|
>|Wand|Needed to focus mana|
>|Arcane word: Alo hamoora|Arcane word that translates to Open mechanism|
>|a swish and flick motion made with the wand|Provides a smooth then sudden movement often needed to unlock a lock|
---
>**Name:** Become Gelatinous
>
>**Outcome:** The caster turns into a jelly like version of themselves. They are able to squeeze through tight cracks absorb impacts, and be sliced and rejoin all without issue. Mobility is cut to 1 foot a minute.
>
> **Mana:** 4
>
>|Componenet|Reason|
>|:---|---:|
>|Wand|Needed to focus mana|
>|Arcane word: Achoo|Arcane word that translates to Slime|
>|Eating a live snail|Helps guide the body into a single uniform gelatinous mass shape|
>|Staying still for 10 minutes|Allows each organ to correctly change consistancy|

---
## Refs Spellbook
Below are the same spells in the refs spellbook. 
>**Name:** Fireball
>
>**Outcome:** A large sphere of fire is grown, and then floats towards my point of choosing, the sphere does damage to items, and people and floats at that point for 15 seconds
>
> **Mana:** 2
>
>|Componenet|Reason|Score|
>|:---|:---:|---:|
>|Staff with a red ruby in the head|Focuses my mana and guids the fireball|+2 Its needed, but nothing special|
>|Arcane word: trizzle|Translates to burning eyes|-1 actually means burn eyes|
>|Black X made with charcoal on the floor|This guides the fireball to the location I want it to go, and allows it to stay floating above the X for the duration|+5 requires planning ahead, charcoal is made from fire and would attract fire, clever|
>
>Componenet Score: 6 x 2 (+2) mana spent, **Roll under 24**
---
## Refs Spellbook
Below are the same spells in the refs spellbook. 
>**Name:** Fireball 2 electric boogaloo
>
>**Outcome:** A large sphere of fire is grown, and then floats towards my point of choosing, the sphere does damage to items, and people and floats at that point for 15 seconds
>
> **Mana:** 2
>
>|Componenet|Reason|Score|
>|:---|:---:|---:|
>|Staff with a red ruby in the head|Focuses my mana and guids the fireball|+2 Its needed, but nothing special|
>|Arcane word: trizzle|Translates to burning eyes|-1 actually means burn eyes|
>|Black X made with charcoal on the floor|This guides the fireball to the location I want it to go, and allows it to stay floating above the X for the duration|+5 requires planning ahead, charcoal is made from fire and would attract fire, clever|
>
>Componenet Score: 6 x 2 (+2) mana spent, **Roll under 24**