# Play Testing Document
## Test 1
### Prelim
Test if Combat Stamina actually does anything/add tactical depth. Skeptical of
effect, seems like needless bookkeeping.

If CS is good, test per round or per combat (per round = 2 or 3 actions per
round which reset at beginning of next round, per combat = current system,
cumulative decrease).

Do % targets feel good? They seem somewhat low, but also low level characters
should be pretty low (maybe relative difference system idk). (This could be
avoided with point "Quicker/deadlier combat".)

Damage feel right? I think so in preliminary solo tests.

HP good? 2 methods, 4d4 or highest 3 of body scores.

Quicker/deadlier combat? Is this a good balance? Potentially increase Def
Targets, add a "defense" reaction, reaction is spent by defender after attacker
succeeds at hitting. Raising def targets will cause more hits, but active
defense adds choice to avoid damage and when to do so. Only one reaction allowed
per turn. If it's spent, cannot react again. Only exception is over 100% or
something. Armor has passive and active def targets. Heavier armor has higher
passive, lower active. Visa versa for lighter armor. Defender rolls against
active, attacker rolls against passive.

### AAR
(date)\

