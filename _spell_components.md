# Spell Componenets

## Common required componenets
|Componenet|Rarity|Magical Connection|properties|modifiers|
|:---|:---:|:---:|:---:|:---:|
|Incantation|Rare|strong|Versitile|Length of phrase, accuracy|
|Hang gestures|uncommon|medium|Focused, silent|Intricasy|
|Staff|Common|Varies|Powerful|Materiel, embeded jewls|
|Potion|Uncommon|Varies|Versitle, Lasting|Ingridients|
|Scroll|Rare|Strong|Versitle, Lasting, Instant|Inks, materials, spells cast|


## Common componenets
|Componenet|Rarity|Magical Connection|properties|modifiers|
|:---|:---:|:---:|:---:|:---:|
|Charcoal|common|weak|Dark, connectd to fire|Made from buring something rare|
|Silver Mirror|Uncommon|mild|Reflective, Divination|Engraved patterns|
|Moonstone|Uncommon|Strong|Lunar, Clarity|Size and purity|
|Stormcloud Vapor|Rare|medium|Electrical, Conducive|Weather conditions|
|Phoenix Feather|Rare|Strong|Fiery, Regeneration|Length of the feather|
|Dragon Scale|Rare|Strong|Elemental, Armor|Age of the scale|
|Ghostly Plasma|Very Rare|Strong|Ethereal, Haunting	|Source of the essence|
|Enchanted Crystal|Rare|Strong|Crystaline, Amplifying|Type of enchantment|
|Celestial Star|Very Rare|Strong|Cosmic, Destiny|Constellation alignment|

## Unique componenets
|Componenet|Rarity|Magical Connection|properties|modifiers|
|:---|:---:|:---:|:---:|:---:|
|Whispered Secrets|Uncommon|Strong|Intrigue, Deception|Nature of the secrets|
|Cast at night|common|Medium|Lunar, Illusion|Phase of the moon|
|Laughter of Fae|Very Rare|Strong|Enchantment, Mischievous|Location of the laughter|
|Written, Paradoxical Riddles|Uncommon|Strange|Maddening, Puzzling|Complexity of the riddle|
|Sickness induced Mirage Reflections|Rare|Strong|Illusory, Disorienting|Nearness to death|
|Betrayaed promise|Rare|Strong|Treachery, Poisonous|Motivation of betrayal|