#!/bin/bash

while getopts "w" flag; do
	case $flag in
		w) watch=1;;
	esac
done
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";

if [[ $watch == 1 ]]; then
	find "$SCRIPT_DIR/." -type f \( -name "*.md" \) | entr sh -c "pandoc --metadata title='FAWCS Booklet I: Player Guide' -H styles.css -s -o dist/output.html player-booklet/*.md"
else
	pandoc --metadata title='FAWCS Booklet I: Player Guide' -H styles.css -s -o dist/output.html player-booklet/*.md
fi
