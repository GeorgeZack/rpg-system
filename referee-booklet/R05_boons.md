# Boons

- Sufficient actions taken and requirements met as appropriate for the
  Birthsign, prior to ceremony
    - Formula...
- Deity finds PC worthy
    - Formula...
- Ref finds a power that the PC would use and is appropriate for the PC

Boons are utilities over time, but each require specific items….
Requirements	Scores (DEX > 10, etc.)	Birthsigns (Must have Banner, etc.)	IPs
spent total	Natural Affinity
				
Ideas below...				Warrior	What’s your STR? Do you qualify?
every 3IPs spent, increase Atk Bonus + 5%				Defender
every 3IPs spent, decrease Def Target - 5%				
				
Decrease IP requirement by 1 if Adventure Dedication				Decrease IP
requirement by 1 if Order Dedication				
