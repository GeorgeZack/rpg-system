# Monsters and NPC Statistics
Defense	HP	Attack	Damage	Move	Morale	Group Size	Treasure	Alignment

Bandit
Bandit, Leader
Bear
Beetle, Giant
Dragon, Young -- -25%
Dragon, Great -- -50%
Ghost
Gnoll
Goblin
Hobgoblin
Kobold
Normal Human -- 55%
Ogre
Orc
Pixie
Snake, Giant
Wolf
Zombie, Lesser
Zombie, Normal
Zombie, Greater
