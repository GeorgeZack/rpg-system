# Conversion Formulas
Money	1gp of BX = 7sp
	1s of Renaissance Dulexe = 3sp
	1 shilling of 12th/13th cen. England = 3sp
THAC0/AC	Base human (no armor) = 55% Defense, no Attack bonus
	THAC0 = Attack, AC = Defense
	AC 9 = Defense 50%, ±1 AC = ±5% change
