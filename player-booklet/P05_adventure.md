# 5 Adventure
Adventure rules are rules used while adventuring---in the Underworld or the
Overworld---but do not include combat [6 Combat].

## 5.1 The Party
The adventuring party is composed of at least one PC---typically more---and can
also include hirelings [5.7 Hirelings] and other hired servants or
professionals. The party is at odds against the monsters and brigands found
throughout the world.

## 5.2 Scale, Time, Travel, and Rest
Adventures are generally broken into two different types: Underworld (in
dungeons, lost temples, abandoned forts, caves, etc) and Overworld (between
settlements, over water, through forests, etc). Rules between the two differ.

### 5.2.1 Underworld
Any exploration in an underground world---such as a dungeon---or ruins, or any
sort of dangerous and unknown place full of potential treasure and adventure is
considered "Underworld." Underworld travel does not necessarily take place
underground, it simply denotes a smaller scale used for exploration of dangerous
and guarded places.

#### 5.2.1.1 Scale
Movement and time is fine grained and is aimed towards meticulous and careful
exploration of Underworld locations.

#### 5.2.1.2 Time
A single turn is 10 minutes of in-game time. Checking for traps, resting, or
loading up treasure takes 1 turn.

#### 5.2.1.3 Travel
The party move and map an area in the dungeon equal to the lowest move rate of a
character in the party in 1 turn. Quickly moving through a dungeon may take less
time depending on the circumstances.

#### 5.2.1.4 Rest
1 turn of rest is required after 5 turns of dungeoneering. So out of 6 turns, 1
must be rest. If rest does not occur, then each character gains 1 level of
fatigue.

#### 5.2.1.5 Light
Candles, lanterns, and torches are vital tools for the party, illuminating
darkness for combatants to see and land accurate hits, as well as facilitating
mapping and exploration.

Light is indispensable in underground labyrinths; without it or darkvision, the
party risks getting lost, needing to roll for each turn without illumination
(see [5.2.1.9 Getting Lost]).

#### 5.2.1.6 Mapping
It is recommended that a player maps while the referee describes the environment
the party is exploring. The corresponding PC of the mapping player will likewise
take time mapping, but this is accounted for in a normal turn of travel, see
[5.2.1.2 Time] and [5.2.1.3 Travel].

#### 5.2.1.7 Traps
Occasionally, the dangers of the Underworld are not monsters or brigands, but
traps. The party is assumed to be checking for traps and the referee will alert
the PCs if such a trap is detected by the PCs. Occasionally the referee may roll
to decide if traps are triggered if the mechanism is particularly sensitive.
Moving while fleeing or without checking for traps, although faster, can also
cause traps to trigger and inflict damage upon the party.

#### 5.2.1.8 Wandering Monsters
Every other turn of Underworld exploration that passes will trigger the referee
to roll for a wandering monster which will encounter the party. The party and
the monster may not notice each other, or one may ambush the other, depending on
the referee, the roll, and the circumstances.

#### 5.2.1.9 Getting Lost
Getting lost is extremely dangerous in a dungeon. More so without light. If the
party is without light there is a slight chance of escape (adjusted by the
referee, taking account of distance and complexity of the path of escape) but
otherwise only death awaits. The base escape chance is 5% (`01-05`) per turn.

### 5.2.2 Overworld
Travel over land, sea, or through the air, and not in any dungeon or ruins is
considered "Overworld". Typically travel is condensed and quicker than the
Underworld.

#### 5.2.2.1 Scale
Movement and time is large-scale and is aimed towards traveling over great
distances in the Overworld.

#### 5.2.2.2 Time
Time passes in the scale of days or at least several hour chunks. Time passes as
the party travels and unless travel is interrupted by an encounter or
distraction, should generally be counted in days or by distance, whichever is
more important to the situation and judged by the referee.

#### 5.2.2.3 Travel
Distance traveled in the Overworld is measured in miles. Typical travel distance
per day is as follows:

| Form of Travel              | Highway | Lesser Road | Cross-Country |
|-----------------------------|---------|-------------|---------------|
| Foot                        | 15 mi   | 15 mi       | ~10 mi        |
| Mounted                     | 30 mi   | 20 mi       | ~12 mi        |
| Mounted with relay stations | 40 mi   | -           | -             |
| Mounted troop/retinue       | 18 mi   | 9 mi        | ~5 mi         |
| Carriage or stagecoach      | 15 mi   | 6 mi        | -             |

Modifiers to speed include hilly or rough terrain and mountainous terrain.
Rough terrain incurs -20% penalty to speed, while mountainous terrain incurs a
-50% penalty. Heavy wheeled vehicles, such as heavy wagons and coaches, will
likely not make it far without getting stuck.

If penalties decrease travel speed to 0, or if the stacked penalties reach
-100%, then travel is not possible until the penalties are removed.

#### 5.2.2.4 Rest
Generally, time taken exploring the overworld will also account for resting.
Resting is factored into travel times. However, forced marches and traveling at
speed over several miles will incur fatigue. Forced marches increase speed by
+50%, but at the end of the day the participants will increase fatigue by 1
level. Fatigue cannot be removed unless the forced march ends and is recovered
like normal.

If traveling while fatigued, the party incurs a different % penalty for each
level of fatigue of the most fatigued individual in the party. Fatigue does not
increase when traveling, the slow speed takes account of reduced ability, but it
does not reduce fatigue. Fatigue must still be recovered like normal.

1. Winded: -10%
2. Tired: -20%
3. Exhausted: -40%
4. Debilitated: Cannot travel and must rest

#### 5.2.2.5 Weather
In addition to road type and terrain type modifiers to speed, weather may also
add penalties to travel. For each day that the inclement weather continues the
penalty is increased.

| Weather              | Route Type | Day 1 of Weather | Day 2 | Day 3 | Day 4 | Day 5+ |
|----------------------|------------|------------------|-------|-------|-------|--------|
| Light Rain           | Road       | -                | -25%  | -50%  | -75%  | Imp.   |
|                      | CC         | -10%             | -25%  | -50%  | Imp.  | Imp.   |
| Heavy Rain           | Road       | -25%             | -50%  | -75%  | Imp.  | Imp.   |
|                      | CC         | -25%             | -50%  | Imp.  | Imp.  | Imp.   |
| Snow or Extreme Heat | Road       | -                | -25%  | Imp.  | Imp.  | Imp.   |
|                      | CC         | -                | -50%  | Imp.  | Imp.  | Imp.   |
| Fog                  | All        | -25%             | -25%  | -25%  | -25%  | -25%   |

CC -- Cross-Country\
Imp. -- Impassable

In order for penalties to be removed, there must be one day of fair weather for
every day the adverse weather persisted. For example, a single day of heavy
rain's penalty is removed *after* a fair day. During the fair day, the current
penalty will persist, but by the end of that fair day, it will be reduced.

#### 5.2.2.6 Night
Night travel is extremely difficult and dangerous. It is recommended that PCs do
not attempt to travel at night unless the party is well prepared or have a
guide. Anyone seen traveling at night could be considered brigands by civilized
observers and will be treated as such. Travel at night incurs a -80% penalty to
speed, unless a clear sky and a full moon assist in which case the penalty is
only -20%.

#### 5.2.2.7 Land
A summary of land travel speeds and penalties:

Foot
: 15 mi/15 mi/~10 mi

Mounted
: 30 mi/20 mi/ ~12 mi

Mounted + relays
: 40 mi (only on highways with such relays)

Mounted troop
: 18 mi/9 mi/ ~5 mi

Carriage
: 15 mi/6 mi (only on roads)

Forced march
: +50%

Rough terrain
: -20%

Mountainous
: -50%

Night
: -80%/-20% (clear and full moon)

Fatigue
:   1. Winded: -10%
    2. Tired: -20%
    3. Exhausted: -40%
    4. Debilitated: Cannot travel and must rest

#### 5.2.2.8 Sea
<!-- TODO -->

#### 5.2.2.9 Air
<!-- TODO -->

#### 5.2.2.10 Getting Lost
While traveling, it is possible for parties to lose relative heading and
position in the world. Every day of cross-country travel in rough or mountainous
terrain the party must roll to determine loss status.

With guide
: 10%

Without guide
: 30%

When lost, the party must spend an entire day searching the local area (and not
progressing towards the intended location) to reorient.

## 5.3 Fatigue
Measured in levels:

1. Winded -- Can run for a short while, -10% to all tests
2. Tired -- Can jog for a short while, -15% to all tests
3. Exhausted -- Only able to walk, move rate is halved, -20% to all tests
4. Debilitated -- Only able to crawl, move rate is almost 0, -50% to all tests

### 5.3.1 Reducing Fatigue
Rest reduces fatigue. Each level of fatigue has different required rest periods
to be reduced:

1. Winded -- 1 hour
2. Tired -- 2 hours
3. Exhausted -- 4 hours
4. Debilitated -- 8 hours

<!-- REWRITE FOLLOWING -->
## 5.4 Saving Throws
TO BE REWRITTEN
<!-- Saving throws are split into four categories: -->
<!---->
<!-- - Reflex -->
<!-- - Endurance -->
<!-- - Power -->
<!-- - Mythic -->
<!---->
<!-- ### 5.4.1 Reflex -->
<!-- Reflex is the avoiding of offensive or projectile spells, jumping out of the way -->
<!-- of sprung traps, or running out of the path of a rock slide or avalanche. -->
<!---->
<!-- ### 5.4.2 Endurance -->
<!-- Endurance is the powering through bad weather, surviving blood loss, resisting -->
<!-- poisons, and generally enduring physical trauma. It also includes the body -->
<!-- avoiding and fighting off vampirism and lycanthropy. -->
<!---->
<!-- ### 5.4.3 Power -->
<!-- Power is the enduring of terror, resisting charms, breaking free from trances, -->
<!-- and overcoming mental trauma. -->
<!---->
<!-- ### 5.4.4 Mythic -->
<!-- Mythic is withstanding mythic or epic monster attacks, enduring dragon breath, -->
<!-- and resisting the petrifying stare of a medusa. -->
<!---->
<!-- --- -->
<!---->
<!-- Some examples follow: -->
<!---->
<!-- >The Ref calls for a Power saving throw after the party wanders into a trapped -->
<!-- >room in a dungeon. Several fail. These characters all see a fantastic and -->
<!-- >horrifying vision, damaging the mind and causing nightmares for days to come. -->
<!---->
<!-- The Power saving throw is straightforward, failure results in a charm terrifying -->
<!-- several characters, causing lost SP, while success results in resisting the -->
<!-- charm effectively. -->
<!---->
<!-- >Mid battle, Ref calls for Reflex saving throws. One character fails while -->
<!-- >another succeeds. The Ref calls for another saving throw, this time Endurance. -->
<!-- >This throw only effects the first, who failed the Reflex throw. The character -->
<!-- >also fails this throw. The first character is hit, taking damage, and then is -->
<!-- >poisoned. This character will likely not last long! -->
<!---->
<!-- The Reflex saving throw is for dodging several poisonous darts flung at the -->
<!-- party by an assassin. The Endurance saving throw determines the potency of the -->
<!-- poison. -->
<!---->
<!-- --- -->
<!-- END REWRITE -->

## 5.5 Health
Only use HP for tracking damage taken by the body, physical toll, etc. When HP
drops to 0, a character becomes weakened. Any further damage dealt after the
character is at 0 results in incapacitation. This causes the character to bleed
out, hemorrhage, or cause organ shutdown, whatever condition is appropriate the
damage dealt and the conditions. Death occurs in `1d6 x 10` minutes after being
incapacitated. This is rolled by the referee. If in the Underworld, each 10
minute interval is translated to a turn, or 60 combat rounds.

Incapacitation simply means that the character is either unconscious, knocked
prone and immobile, or the equivalent status. The character cannot take further
actions until recovered, either retrieved by allies or healed on the spot. Any
damage dealt to an incapacitated character can be considered mortal and will
cause death immediately.

### 5.5.1 Healing
<!-- TODO -->

### 5.5.2 Death
Death is the end of many adventurers, and the inevitable end to all. Make it
worthwhile. Once a character is dead, return the record sheet to the referee and
begin creating a new character.

Upon adjudication from the referee, a dead character may return in a different
form. Such form and process is described in [5.10 The Spirit World].

## 5.6 Sanity
Use SP for tracking mental damage taken by the mind. When SP drops to 0, the
player must roll to see which condition the character gains.

| Roll    | Result         |
|---------|----------------|
| `01-20` | Paranoia       |
| `21-40` | Hallucinations |
| `41-60` | Catatonia      |
| `61-80` | Mania          |
| `81-00` | Dissociation   |

<!-- Descriptions and results of each... -->

When 

### 5.6.1 Recovery
Recovering from a sanity condition

### 5.6.2 Insanity
Insanity is not the inevitable end to all, and is an unfortunate end to some. It
is the equivalent to death. Once a character becomes insane, the mind is
considered completely shattered and irrecoverable. Return the record sheet and
begin creating a new character.

However, depending on the referee, an insane or broken character may return with
limitations. The process of return is as follows:

1. Character must be recovered (physically) by the rest of the party
2. Character must be sent to a recovery place (such as a monastery or asylum)
3. Every month in recovery, the player may roll one Power saving throw with a
   penalty of -90% but every month the penalty is reduced by 5%, success allows
   the character to return to action
4. The character must spend---through generous donations of course---10% of
   total wealth to the institution (if the character has no wealth the referee
   can determine what a "cost" may be, such as a quest or debt)

## 5.7 Aging
<!-- TODO -->

## 5.8 Hirelings
<!-- TODO -->

## 5.9 Fealty
<!-- TODO -->

### 5.9.1 Score
<!-- TODO -->

### 5.9.2 Changes in Score
<!-- TODO -->

### 5.9.3 Role
<!-- TODO -->

### 5.9.4 Duties
<!-- TODO -->

### 5.9.5 Losing Fealty
<!-- TODO -->

### 5.9.6 Changing Fealty
<!-- TODO -->

## 5.10 The Spirit World
<!-- TODO -->
