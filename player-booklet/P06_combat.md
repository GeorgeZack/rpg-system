# 6 Combat
## 6.1 Overview
Combat is deadly. Armor provides insurance to a character, allowing a hit to be
negated. Firearms, however, blast through armor but take several rounds to
reload. It is likely that in close combat a single shot is all that a combatant
can get off before making contact.

### 6.1.1 Encounter Distance
Referee determines the distance of combat, either a far (ranged) encounter or a
close encounter. Usually in the Underworld it will depend on if the two parties
have identified each other, location in the Underworld (such as around a corner
or down the hall), and if either party intends on an ambush.

### 6.1.2 Morale
Each monster and NPC is assigned a morale score, represented as a percentage.
Morale checks are conducted three times during encounters involving the
non-player party. In this context, a "combatant" refers to any member of the
non-player party engaged in combat.

1. The first morale check occurs when a combatant is killed or incapacitated
2. The second morale check is triggered when half of the combatants are killed
   or incapacitated
3. The third morale check takes place when a leader within the non-player party
   is killed or incapacitated.

During morale checks, the referee rolls a `1d100` against the corresponding
morale score. Failure indicates that the enemies retreat, capitulate, or
surrender to the PC group.

### 6.1.3 Combat Stamina
During combat, combatants will spend stamina to perform actions. Each action has
an associated cost.

## 6.2 Procedure
### 6.2.1 Time
1 round = 10 seconds\
1 (Underworld) turn = 60 rounds

Combat encounters round up to the nearest turn, 1-60 rounds = 1 turn, 61 rounds
= 2 turns, etc.

An attack action represents a duel between two combatants where one
combatant takes initiative and takes the active stance in the combat. It does
not represent a single swing of a weapon. A failure to do damage may very
well mean that the two combatants duel and do not make any effective
fighting. Each block and parry and the round is not decided.

### 6.2.2 Reaction
If neither party has ambushed the other, or a clear advantage over the other,
then both sides will roll to see which side, if any, gets to ambush the other.
If both sides fail the roll, then combat is started with an initiative roll.
Otherwise, the winning side gets a free combat round. Each side will roll
against the highest BLANK score found in the group.

### 6.2.3 Initiative
`1d10`

Each side in battle rolls for initiate order, rerolling after each round. The
lowest roll goes first. Ties can be rerolled or considered simultaneous.

### 6.2.4 Defense Target
Defense target is the target to which a combatant must test against. It can be
hidden---and perhaps best if it is---by the referee.

### 6.2.5 Attack Bonus
Always can hit on an automatic success, or at least a `01-05`. Fail on `96-00`.

### 6.2.6 Reloading
Ranged weapons or firearms require a reload to be fired again. The weapon
details have information about how many rounds it takes to reload. If a
character elects to reload but is interrupted during the process, the time spent
is lost, the reload must restart.

## 6.3 Actions
### 6.3.1 Major Actions

- Normal attack; 1 CS
- Focused attack: -20% Attack Bonus, success allows attacker to pick which body
  part to damage; 1 CS
- Daring attack: +20% Attack Bonus, but +20% Defense Target; 1 CS
- Charge (counts as both move and major action): Requires minimum of 10ft and
  maximum of (Move Rate x 2)ft; Add 1d6 damage; 2 CS
- Brace: -20% Defense Target against charges; free attack when charged; bonus
  lost when any other action is taken; 1 CS
- Cast Spell; 1 CS
- Quick Cast Spell: -20% to roll?; can take another major action; 0 CS
- Charge spell: Delay spell cast until Nth round; +10% per Nth round delayed to
  roll?; 1 CS
- Help: Stopping an ally from bleeding, pushing ally out of harms way, helping
  ally brace, etc.; target gets +20% Attack Bonus or -20% to Defense Bonus,
  determined by referee; 1 CS
- Switch or Ready weapon: Replace current weapon with accessible weapon; Ready
  weapon that is not accessible if currently unarmed; 0 CS
- Reload: Spend the round (including Forfeit movement) to reload weapon; 0 CS
- Rest: Forfeit movement action; regain 1d4 CS up to maximum CS

### 6.3.2 Movement Actions

- Move: Up to move rate; 0 CS
- Sprint, Swim, or Climb: Forfeit major action, move up to double move rate; 1
  CS
- Charge: See major action; 2 CS
- Reload: See major action; 0 CS

### 6.3.3 Minor Actions
All are 0 CS;

- Interact with object
- Pick up object
- Drop object
- Ready accessible weapon

## 6.4 Special Combat
### 6.4.1 Grappling
Move, disarm, bind, etc, characters and monsters.

#### 6.4.1.1 Actions
<!-- TODO -->

### 6.4.2 Hand to Hand
Damages CS instead of HP. When certain thresholds are reduced, exhaustion level
increases.

#### 6.4.2.1 Actions
<!-- TODO -->

## 6.5 Damage and Health
All damage is done by a roll of `1d12` unless otherwise noted. When HP drops to
0 or below 0, any further damage will incapacitate or kill the target depending
upon amount of damage dealt.

### 6.5.1 Minor and Major Injuries
An attack that is more than half of the total health (rounded down) of the
target is considered a Minor/Major Injury. A Minor Injury is when the large
damage is dealt above 0 HP, while a Major Injury is when the damage brings the
target down to 0 or below 0 HP.

### 6.5.2 Hitting Armor or Flesh
<!-- TODO -->

### 6.5.3 Quick Chart
<!-- TODO -->
| Roll | Location        | Minor | Major |
|------|-----------------|-------|-------|
|      | Head            |       |       |
|      | Head (face)     |       |       |
|      | Torso (chest)   |       |       |
|      | Torso (stomach) |       |       |
|      | Torso (groin)   |       |       |
|      | Torso (back)    |       |       |
|      | Left Arm        |       |       |
|      | Right Arm       |       |       |
|      | Left Leg        |       |       |
|      | Right Leg       |       |       |

### 6.5.4 Head
<!-- TODO -->

### 6.5.5 Torso
<!-- TODO -->

### 6.5.6 Arms
<!-- TODO -->

### 6.5.7 Legs
<!-- TODO -->

### 6.5.8 Fumbles
<!-- TODO -->

### 6.5.9 Malfunctions
<!-- TODO -->

### 6.5.10 Incapacitation and Death
<!-- TODO -->

## 6.6 Theater of the Mind
### 6.6.1 Distance
<!-- 0 = grappling, 1 = close melee, 2 = far melee, 3 = short range, 4 = far
range, 5 = contact -->

### 6.6.2 Movement
<!-- Movement Rate / 10 = MR points -->

