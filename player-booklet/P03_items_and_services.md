# 3 Items and Services
## 3.1 Encumbrance
Encumbrance is a combination of weight and bulk. It is measured in whole units.
It is represented by a number followed by a \# (number sign or pound sign)
symbol.

If an item has no encumbrance value, then it is assumed that a dozen items will
net the total encumbrance by 1 (12 no Enc items = 1 Enc bundle).

## 3.2 Coinage

| Coinage             | Value        | Size | Enc^1^ |
|---------------------|--------------|------|--------|
| Gold Piece/Crown    | 1            | 1    | 100    |
| Silver Piece/Silver | 1/20         | 1    | 100    |
| Half Piece/Halfer   | 1/40         | 1/3  | 200    |
| Strange Coin        | `1d6 x 1`gp  | 2    | 50     |
| Glowing Coin        | `1d12 x 1`gp | 2    | 300    |

^1^ Number of coins per 1#

## 3.3 Armor

| Armor              | Armor Type  | Defense Bonus | Combat Stamina | Damage Reduction | Move Rate^2^ | Enc | Cost     |
|--------------------|-------------|---------------|----------------|------------------|--------------|-----|----------|
| Cloth and Leather  | Light       | -10%          | 10             | 1                | 40           | 4   | 5sp      |
| Gambeson           | Light       | -10%          | 12             | 1                | 40           | 3   | 8sp      |
| Boiled Leather     | Light       | -10%          | 10             | 1                | 40           | 4   | 12sp     |
| Brigadine          | Medium      | -20%          | 9              | 3                | 30           | 6   | 50sp     |
| Reinforced Leather | Medium      | -20%          | 10             | 3                | 30           | 5   | 60sp     |
| Ring Mail          | Medium      | -20%          | 9              | 4                | 30           | 6   | 150sp    |
| Half Plate         | Medium      | -30%          | 8              | 4                | 30           | 5   | 1,500sp  |
| Full Mail          | Heavy       | -30%          | 6              | 5                | 20           | 12  | 300sp    |
| Scale/Lamellar     | Heavy       | -30%          | 7              | 5                | 20           | 9   | 850sp    |
| Partial Plate      | Heavy       | -30%          | 8              | 6                | 20           | 8   | 2,000sp  |
| Full Plate         | Super Heavy | -40%          | 7              | 7                | 20           | 10  | 6,000sp  |
| Fitted Full Plate  | Super Heavy | -40%          | 8              | 7                | 30           | 9   | 10,000sp |
| Buckler            | Shield^1^   | -5%           | -              | 1                | -            | 1   | 10sp     |
| Medium Shield      | Shield^1^   | -10%          | -              | 2                | -            | 2   | 30sp     |
| Tower Shield       | Shield^1^   | -15%          | -              | 3                | -            | 3   | 70sp     |

^1^ Provides an additional bonus to armor\
^2^ Move rate in a round, measured in feet

### 3.3.1 Descriptions
<!-- TODO -->

### 3.3.2 Magic Armor
<!-- TODO -->

## 3.4 Weapons
All weapons, unless otherwise noted, roll `1d12` for damage.

#### 3.4.1 Melee Weapons

| Weapon           | Weapon Type  | Notes                      | Enc | Cost         |
|------------------|--------------|----------------------------|-----|--------------|
| Axe              | Axe          | Tool, Thrown               | 1   | 6sp          |
| War axe          | Axe          | -                          | 1   | 12sp         |
| Throwing axe     | Axe          | Thrown                     | 1   | 10sp         |
| Great axe        | Axe          | Long                       | 2   | 50sp         |
| Double-Sided Axe | Axe          | Long, Heavy                | 4   | 70sp         |
| Dagger           | Dagger       | Thrown                     | 1   | 5sp          |
| Knife            | Dagger       | Small, Tool, Thrown        | 1   | 1sp          |
| Swordbreaker     | Dagger       | Special                    | 1   | 30sp         |
| Mace             | Mace         | Blunt                      | 1   | 30sp         |
| Hammer           | Mace         | Tool, Blunt                | 1   | 5sp          |
| Cudgel           | Mace         | Small, Blunt               | 1   | 4sp          |
| Club             | Mace         | Blunt                      | 1   | 10sp         |
| Military Pick    | Mace         | Stab                       | 1   | 50sp         |
| Great hammer     | Mace         | Long, Heavy, Blunt         | 4   | 80sp         |
| Whip             | Misc.        | -                          | 1   | 10sp         |
| Net              | Misc.        | Special                    | 2   | 15sp         |
| Polearm          | Polearm      | Cut                        | 2   | 75sp         |
| Javelin          | Polearm      | Small, Thrown              | 1   | 8sp          |
| Staff            | Polearm      | Blunt                      | 2   | 6sp          |
| Spear            | Polearm      | Thrown                     | 2   | 10sp         |
| Morning Star     | Polearm/Mace | -                          | 2   | 45sp         |
| Poleaxe          | Polearm/Axe  | -                          | 2   | 45sp         |
| Pike             | Polearm      | Long                       | 4   | 15sp         |
| Lance            | Polearm      | Cavalry                    | 3   | 45sp         |
| Man-catcher      | Polearm      | Special                    | 2   | 20sp         |
| Sword            | Sword        | -                          | 2   | 20sp         |
| Short sword      | Sword        | Small                      | 1   | 45sp         |
| Rapier           | Sword        | Quick, Stab                | 1   | 30sp         |
| Curved Sword     | Sword        | Cut, Quick^1^or Cavalry^1^ | 2   | 60sp/40sp^2^ |
| Longsword        | Sword        | Long                       | 2   | 75sp         |
| Greatsword       | Sword        | Long, Heavy                | 4   | 100sp        |

^1^ Quick and Cavalry are exclusive, depends on weapon\
^2^ Cheaper if not using Quick/Cavalry

#### 3.4.2 Ranged Weapons

| Weapon           | Weapon Type | Range      | Reload | Notes         | Enc | Cost    |
|------------------|-------------|------------|--------|---------------|-----|---------|
| Hunting bow      | Bow         | 75yd       | 1      | -             | 1   | 30sp    |
| Short bow        | Bow         | 75yd       | 1      | Small         | 1   | 45sp    |
| Long bow         | Bow         | 150yd      | 1      | Long          | 2   | 60sp    |
| Recurve bow      | Bow         | 125yd      | 1      | Cavalry       | 1   | 60sp    |
| Crossbow         | Crossbow    | 130yd      | 2      | -             | 1   | 60sp    |
| Hunting crossbow | Crossbow    | 100yd      | 2      | Small         | 1   | 50sp    |
| Siege crossbow   | Crossbow    | 150yd      | 2      | Heavy         | 2   | 80sp    |
| Arquebus^1^      | Firearm     | 10yd       | 6      | Cavalry       | 2   | 30sp    |
| Carbine          | Firearm     | 12yd       | 3^2^   | Cavalry       | 2   | 30sp^3^ |
| Donderbus        | Firearm     | 5yd        | 3^2^   | -             | 2   | 20sp^3^ |
| Pistol           | Firearm     | 5yd        | 2^2^   | Small         | 1   | 20sp^3^ |
| Duelling Pistol  | Firearm     | 10yd       | 2^2^   | Small         | 1   | 45sp^3^ |
| Grenade          | Firearm     | 10yd/5yd^4^ | 2      | Explosive     | 1   | 3sp     |
| Musket           | Firearm     | 30yd       | 5^2^   | Long          | 3   | 10sp^3^ |
| Petard           | Firearm     | -          | -      | Explosive     | 1   | 3sp     |
| Rifle^5^         | Firearm     | 45yd       | 5      | Heavy, Rifled | 2   | 30sp    |
| Sling            | Misc.       | 50yd       | 1      | -             | -   | 1hp     |
| Blowgun/Dartgun  | Misc.       | 10yd       | 1      | Special       | -   | 5sp     |
| Javelin          | Polearm     | See note   | 1      | Small, Thrown | 1   | 8sp     |
| Arrow            | Projectile  | -          | -      | -             | -   | 1sp 1hp |
| Ball             | Projectile  | -          | -      | Piercing      | -   | 1sp 1hp |
| Dart             | Projectile  | -          | -      | Small         | -   | 1sp     |
| Bolt             | Projectile  | -          | -      | -             | -   | 1sp     |
| Stone            | Projectile  | -          | -      | -             | -   | 1hp     |

^1^ Only available as a matchlock mechanism\
^2^ Decrease reload time by 1 if not using matchlock mechanism\
^3^ Price given for matchlock mechanism\
^4^ First number is throw range, second is explosion radius\
^5^ Only available as a flintlock mechanism

Each standard firearm uses one of three mechanisms: Matchlock, Flintlock, and
Wheellock. The matchlock is the earliest and depending on technology could be
phased out. The wheellock is the next innovation after the matchlock and is more
expensive but more reliable. The flintlock is the final innovation being
slightly more expensive than wheellock and more reliable. Use the following
to determine cost if a price is given for matchlock only:

- Wheellock: 1.5x base cost
- Flintlock: 2x base cost

Artillery is also available to purchase and use, though a single individual
purchasing a piece might be rare.

<!-- TODO: Artillery table, rules about hauling pieces -->

### 3.4.3 Descriptions

| Type      | Damage Roll              | Attack Bonus             |
|-----------|--------------------------|--------------------------|
| Blunt     | -                        | +5% vs armor             |
| Cavalry   | -                        | +5% on horseback         |
| Cut       | +1 vs unarmored opponent | -                        |
| Explosive | +1/2 of roll result      | -                        |
| Heavy     | +1                       | -                        |
| Long      | +1 in open space         | -5% in tight space       |
| Piercing  | Ignores armor            | -                        |
| Quick     | -                        | +5% vs non-quick weapons |
| Rifled    | -                        | +10%                     |
| Small     | -1                       | -                        |
| Special   | -                        | -                        |
| Stab      | +1 vs armor opponent     | -                        |
| Tool      | -                        | -                        |
| Thrown    | -                        | -                        |

Thrown weapons have a range of... TODO

<!-- TODO -->

### 3.4.4 Initiative
Initiative order (melee)

1. Polearms
2. Axe, Mace, Sword, Misc.
3. Dagger

Initiative order (ranged)

1. Crossbows, Firearms
2. Bows, Misc.
3. Thrown

### 3.4.5 Silvered Weapons
Silvered weapons or projectiles are costly and difficult to acquire. Multiply
the cost by **3**.

### 3.4.6 Reload
<!-- TODO -->

## 3.5 General Equipment

| Item                      | Enc    | Cost    | Notes                                   |
|---------------------------|--------|---------|-----------------------------------------|
| Backpack                  | 1^1^   | 20sp    | ENC Load: 15                            |
| Bedroll                   | 1      | 3sp     | -                                       |
| Bottle                    | 1/5    | 1sp     | -                                       |
| Candle - Cheap            | 1/5    | 1hp     | Light: 3ft, 6hrs                        |
| Candle - Fine             | 1/5    | 1sp     | Light: 3ft, 6hrs                        |
| Crowbar                   | 1      | 1sp 1hp | Hammer, -10%                            |
| Cooking kit               | 5      | 60sp    | -                                       |
| Flint and Tinder          | 1/5    | 2hp     | -                                       |
| Garlic                    | -      | 1hp     | -                                       |
| Holy symbol - Simple      | -      | 1hp     | -                                       |
| Holy Symbol - Fine        | -      | 1sp     | -                                       |
| Holy Symbol - Extravagant | -      | 20sp    | -                                       |
| Holy water                | 1      | 1sp     | -                                       |
| Iron spikes               | 1      | 1sp 1hp | Pack of 10                              |
| Ink                       | 1      | 10sp    | -                                       |
| Lantern                   | 1      | 4sp 1hp | Light: 15ft                             |
| Mirror                    | 1/5    | 2sp     | -                                       |
| Oil                       | 1      | 1sp 1hp | Light: 2hrs                             |
| Pickaxe                   | 1      | 4sp 1hp | Mace, -10%                              |
| Pipe                      | -      | 1hp     | -                                       |
| Pole                      | 2      | 4sp     | Length: 10ft                            |
| Rope - Hemp               | 2      | 12sp    | Length: 50ft                            |
| Rope - Silk               | 1      | 30sp    | Length: 50ft                            |
| Sack - Small              | 1/2^1^ | 1sp     | ENC load: 3                             |
| Sack - Large              | 1^1^   | 3sp     | ENC load: 6                             |
| Scythe                    | 3      | 4sp     | Axe, -20%                               |
| Shovel                    | 1      | 3sp 1hp | Staff, -10%                             |
| Tent - Small              | 3      | 48sp    | 2 person capacity                       |
| Tent - Large              | 6      | 60sp    | 6 person capacity                       |
| Torch                     | 1/3    | 1sp     | Light: 10ft, 1hr; Club, -10%, `1d4` dmg |
| Waterskin - Small         | 1      | 3sp     | 1 day capacity                          |
| Waterskin - Large         | 2      | 8sp     | 3 day capacity                          |
| Waterskin - Animal        | 4      | 10sp    | 1 day capacity for a large animal       |
| Wolfsbane                 | -      | 1hp     | -                                       |
| Whetstone                 | 1/5    | 1sp     | -                                       |

^1^ These items hold a load. Each has its own encumbrance, but will allow a
character to carry even more items (and therefore encumbrance).

### 3.5.1 Descriptions

- Backpack -- Used to haul many items and can be securely strapped to the back
  of a character. Has various pockets and straps for items.
- Blanket -- A blanket and pad to sleep on while on adventure
- Bottle -- Empty glass bottle, holds a small amount of liquid
- Candle -- Provides illumination up to 3ft for 6 hours
    - Cheap -- Made of animal fat
    - Fine -- Made of beeswax and burns cleanly
- Crowbar -- Tool for leveraging or prying objects; Use Hammer for weapon stats,
  -10% penalty
- Cooking -- Cumbersome cooking set with kettle, pans, spit, etc.
- Flint -- Used to start a small fire
- Garlic -- Said to have special properties against vampyres, undead, and other
  creatures of the night
- Holy Symbol -- Symbol of significance to a religion or order, usually no
  larger than the palm of a hand
    - Simple -- A simple symbol is made of wood or similar materials
    - Fine -- Made of metal or wood but decorated with silver gilding
    - Extravagant -- Made of metal and decorated with gems or gold gilding
- Holy Water -- Comes in a bottle
- Iron spikes -- Used to bar doors closed or keep them open
- Ink -- Used for writing with a quill
- Lantern -- Provides illumination up to 15ft, time depends on fuel, resistant
  to being extinguished
- Mirror -- Small metal hand mirror
- Oil -- Comes in a bottle and provides fuel for a lantern for 2 hours
- Pickaxe -- Mining tool; Use Mace for weapon stats, -10% penalty
- Pipe -- Made of clay, for smoking
- Pole -- A collapsible 10ft pole made of wood
- Rope -- Rope that comes in 50ft length
    - Hemp -- Strong hemp material
    - Silk -- Silken rope which is lighter than hemp
- Sack -- Used to haul small items such as coins or treasure. A large sack
  requires both hands to carry, while a small sack can be tied to a backpack
  or belt loop.
- Scythe -- Farming tool; Use Axe for weapon stats, -20% penalty
- Shovel -- Simple digging tool; Use Staff for weapon stats, -10% penalty
- Tent -- Temporary shelter for adventurers
- Torch -- A stick with a flammable end that provides illumination up to 10ft
  for 1 hour; Use Club for weapon stats, -10% penalty, `1d4` damage.
- Waterskin (or Wineskin) -- A receptacle used to hold water, made from leather
  or animal skin
    - Small or personal -- Holds enough water to sustain a character for 1 day
    - Large -- Holds enough water to sustain a character for 3 days
    - Animal -- A very large skin that can be opened up to allow animals to
      drink from, can sustain a pack/large animal for 1 day
- Wolfsbane -- Said to have special properties against wolves, lycanthropes, and
  other beastkin
- Whetstone -- Used to sharpen and polish blades or tools

## 3.6 Provisions

| Item                    | Enc | Cost | Notes             |
|-------------------------|-----|------|-------------------|
| Rations - Normal        | 4   | 1sp  | 1 person, 1 week  |
| Rations - Preserved     | 3   | 25sp | 1 person, 1 week  |
| Rations - Magic/Blessed | 2   | 50sp | 1 person, 2 weeks |
| Ale                     | -   | 1sp  | 1 pint            |
| Wine                    | -   | 3sp  | 1 pint            |
| Must                    | -   | 2sp  | 1 pint            |

### 3.6.1 Descriptions

- Rations -- Food prepared for travel
    - Normal -- Non-preserved food; feeds 1 person for 1 week
    - Preserved -- Preserved food; feeds 1 person for 1 week
    - Magic or Blessed -- Magically preserved food or blessed food; feeds 1
      person for 2 weeks
- Ale -- A pint of average brewed ale
- Wine -- A pint of average fermented fruit
- Must -- A pint of average non-fermented fruit juice

### 3.6.2 Filling a Wineskin
Multiply the cost by **8**.

## 3.7 Specialist Equipment

| Item                            | Enc | Cost  |
|---------------------------------|-----|-------|
| Alchemy Kit                     | N/A | 900sp |
| Climbing Kit                    | 1   | 60sp  |
| Fishing Tools                   | 2   | 25sp  |
| Healers Kit                     | 1   | 90sp  |
| Musical Instrument - Percussion | 1   | 10sp  |
| Musical Instrument - Pipe       | 1   | 30sp  |
| Musical Instrument - String     | 2   | 45sp  |
| Musical Instrument - Wind       | 1/2 | 6sp   |
| Thieves Tools                   | 1   | 60sp  |
| Writing Kit                     | 1   | 60sp  |

### 3.7.1 Descriptions

- Alchemy Kit -- Includes bottles, jack box, pestle and mortar, a portable oven,
  a large tent, benches, and everything needed to create complicated potions;
  must be hauled by a mule or other pack animal
- Climbing Kit -- Includes rope, grappling hook, leather harness, pitons, and
  other climbing tools
- Fishing Tools -- Everything needed to fish, includes pole, lures, string, and
  a net
- Healers Kit -- Includes herbs and tools to prepare simple remedies including a
  pestle and mortar
- Musical Instruments
    - Percussion -- A small handheld percussion instrument
    - Pipe -- A small bagpipe or similar, tubular, instrument
    - String -- A delicate instrument utilizing strings
    - Wind -- A simple and small blown instrument
- Thieves Tools -- Kit for breaking and entering, includes lockpicks, probes,
  and skeleton keys
- Writing Kit -- Quills, ink, and paper

## 3.8 Starting Equipment Packs
<!-- TODO -->

### 3.8.1 Descriptions
<!-- TODO -->

## 3.9 Services
<!-- TODO -->

| Service               | Cost |
|-----------------------|------|
| Alehouse              |      |
| Coaching Inn          |      |
| High Society Club     |      |
| Lodging-house         |      |
| Cheap accommodations  |      |
| Normal accommodations |      |
| Rich accommodations   |      |

### 3.9.1 Descriptions
<!-- TODO -->

## 3.10 Hirelings
<!-- TODO -->

### 3.10.1 Descriptions
<!-- TODO -->

## 3.11 Transportation
<!-- TODO -->

### 3.11.1 Descriptions
<!-- TODO -->

