# 2 Character Creation
## 2.1 Procedure

<!-- 1. Roll `1d100`, eight times, each roll being used to determine initial -->
<!--    statistics (using table 1), while noting the roll result is next to the -->
<!--    attribute: -->
<!--     - Strength `STR` -->
<!--     - Dexterity `DEX` -->
<!--     - Endurance `END` -->
<!--     - Intelligence `INT` -->
<!--     - Personality `PER` -->
<!--     - Wisdom `WIS` -->
<!--     - Looks `LKS` -->
<!--     - Soul `SOL` -->
<!-- 2. Note each bonus (from table 1) -->
<!-- 3. Choose a race -->
<!-- 4. Choose a background (examples found in table 2) -->
<!--     - Roll `1d6` or choose to determine age (see table 2 for age bracket and -->
<!--       table 3 for specific ages) -->
<!--     - Roll `1d100` to determine success in background (see table 4 for -->
<!--       information), taking into account time spent -->
<!-- 5. Choose calling (Adventure, Order, Magic) -->
<!-- 6. Roll `4d4` hit points -->
<!-- 7. Roll `4d4` sanity points -->
<!-- 8. Roll starting wealth dice (defined in table 1, under **LKS**) -->
<!-- 9. Purchase starting equipment -->
<!-- 10. Note Attack Bonus, Defense Target, Combat Stamina, and Damage Reduction -->
<!--     after purchasing equipment and determining calling -->
<!-- 11. Determine alignment -->
<!--     - Law -->
<!--     - Neutral -->
<!--     - Chaos\* -->
<!-- 12. Determine languages known (examples found in table 5) -->
<!-- 13. Fill out final details, such as looks, goals, family, etc. -->
<!---->
<!-- \* -- It is not recommended to have Chaos PCs. If PCs wish to add nuance to Law -->
<!-- or Neutral, Fealty may be utilized. -->
1. Roll `1d100` for each attribute and note score
    A.  The attributes are:
        1.  Strength
        2.  Dexterity
        3.  Endurance
        4.  Looks
        5.  Intelligence
        6.  Wisdom
        7.  Personality
        8.  Soul
2. Roll family social class
    A.  Choose trade within social class
3. Enter trade
    A.  Roll for survival
        I.  If calamity, roll for savagery
        II.  If death, restart process with a free reroll
    B.  If completing 4+ eras, roll aging
    C.  Roll for prosperity
    D.  Choose from trade benefits table
    E.  If continuing trade, return to **3A** adding 4 years, else move to **4**
4. Retire from trade
    A.  Roll retirement payout
    B.  Choose "rise"
        I.  Independent - No debt, no benefits
        II.  Indentured - Large debt, extra benefits, starting capital
        III.  Studied - Costs 10,000sp, extra benefits, start with access to
              mentor or building associated with trade
    C.  Roll final benefits if any
    D.  Make final purchases
5. Write characteristics, background, equipment, wealth (and or debt) on record
   sheet

### Attribute Table

| Attribute Roll | `01-03` | `04-15` | `16-36` | `37-64` | `65-85` | `86-97` | `98-00` |
|----------------|---------|---------|---------|---------|---------|---------|---------|
| Score          | 7       | 6       | 5       | 4       | 3       | 2       | 1       |

### Social Class Table

| Roll    | Social Class |
|---------|--------------|
| `01-10` | Nobility     |
| `11-25` | Gentry       |
| `26-45` | Middle Class |
| `46-70` | Urban        |
| `71-00` | Peasant      |

### Trades Table

| Trades                  | Social Class Eligibility | Survival |
|-------------------------|--------------------------|----------|
| Agriculture             | P, G                     | Physical |
| Amateur Soldiering      | P, U                     | Physical |
| Arcanum                 | M, G, N                  | Sanity   |
| City Watch              | U, M                     | Physical |
| Clergy                  | All                      | Sanity   |
| Clerk                   | M, G                     | Physical |
| Commerce                | U, M, G, N               | Physical |
| Craft                   | P, U, M                  | Physical |
| Crime                   | All                      | Physical |
| Domestic Service        | P, U                     | Physical |
| Entertainment           | P, U, M                  | Physical |
| Healing                 | All                      | Physical |
| Inquisition             | All                      | Sanity   |
| Maritime                | All                      | Physical |
| Mercenary Service       | All                      | Physical |
| Militia                 | P, U                     | Physical |
| Politics                | G, N                     | Sanity   |
| Professional Soldiering | M, G, N                  | Physical |
| Scholarship             | M, G, N                  | Sanity   |
| Smith                   | P, U, M                  | Physical |
| Wood Keeping            | P, G                     | Physical |

### Survival Table

| Survival Type | Calamity Chance |
|---------------|-----------------|

### Calamity Table

| Type   | Result | Death Chance |
|--------|--------|--------------|
| Famine |        |              |

The potential  of a character within the chosen trade is influenced and
sometimes limited by Social Class. Although a Noble and a Peasant may be in the
Inquisition, the Nobility would be in a leadership role while the Peasantry
would be in a labor role. In like manner, Gentry would not perform the labor of
finding or burning witches, but rather would oversee the task on behalf of the
Nobility.

### Benefits Table

| Roll  | Nobility              | Gentry             | Middle Class       | Urban              | Peasant            |
|-------|-----------------------|--------------------|--------------------|--------------------|--------------------|
| 01-25 | Melee, up to 80sp     | Melee, 50sp        | Melee, 30sp        | Melee, 15sp        | Melee, tool        |
| 26-50 | Ranged^1^, up to 80sp | Ranged^1^, 50sp    | Ranged^1^, 30sp    | Ranged^1^, 15sp    | Ranged^1^, 10sp    |
| 51-70 | Musket (Flintlock)    | Musket (Wheellock) | Musket (Matchlock) | Pistol (Matchlock) | Pistol (Matchlock) |
| 71-85 | War-Horse             | Horse              | Donkey             | Backpack           | Backpack           |
| 86-96 | Alchemy Kit^2^        | Writing Kit        | Healers Kit        | Thieves Tools      | Musical Instrument |
| 96-00 | Partial Plate         | Half Plate         | Reinforced Leather | Gambeson           | Gambeson           |

^1^ Excluding firearms\
^2^ Pack animal not provided

### Connection Table

| Roll  | Nobility           | Gentry               | Middle Class              | Urban                   | Peasant                |
|-------|--------------------|----------------------|---------------------------|-------------------------|------------------------|
| 01-20 | High-ranking noble | Wealthy merchant     | Successful business owner | Prominent guild member  | Respected elder        |
| 21-40 | Court official     | Local magistrate     | Town councilor            | City official           | Village elder          |
| 41-60 | Military commander | Captain of the guard | Militia leader            | City watch captain      | Village militia leader |
| 61-80 | Renowned scholar   | Respected teacher    | Skilled artisan           | Knowledgeable tradesman | Wise farmer            |
| 81-00 | Famous artist      | Talented musician    | Creative craftsman        | Popular entertainer     | Skilled storyteller    |

## 2.2 Attributes

---

Designers Note:\
Eight attributes represent a character's physical and mental ability. Eight may
seen like a lot compared to other systems, but there are no skills here.
Characters may look the same statistically and according to these attributes,
but that is okay. Differences in character do not need to be expressed in the
stats, instead play style, player ability, equipment, special training,
background, wealth, debt, fealty, and past actions in game determine
differences.

Keeping the stats simple and avoiding skills allows for faster character
creation and less mental load on the referee during play, hopefully allowing
more fun to be had rather than rules rote. Lack of skills also help imagination
and innovation flourish while discouraging "checking the control panel"
character sheet, so to speak, for which skills exist and "guarantee" success.

---


### 2.2.1 Strength
Strength is the physical power and capability of a character, including muscle
strength and raw physical force.

### 2.2.2 Dexterity
Dexterity is the agility, nimbleness, coordination, and speed of a character.

### 2.2.3 Endurance
Endurance is the stamina, resilience of a character and represents the ability
to withstand physical strain and hardship.

### 2.2.4 Looks
Looks is the attribute of physical attraction and presence of a character,
encompassing appeal and how others will think of the character.

### 2.2.5 Intelligence
Intelligence is the mental acuity, cognitive ability, and the character's
ability to reason and understand situations.

### 2.2.6 Wisdom
Wisdom is the insight, intuition, and practical understanding of a character,
and includes the ability perceive truths and exercise sound judgment.

### 2.2.7 Personality
Personality is the charisma, social intuition of a character and includes the
ability to interact well with others.

### 2.2.8 Soul
Soul is the magickal, spiritual self that resides in each character and includes
magickal resistance, aptitude, and potential.

## 2.3 Demi-Humans
+/- to each atr, limit IPs earned frequency.

### 2.3.1 Rarity of Demi-Humans
Ref determines probability of each, player rolls d% to determine qualification.
Demi-humans can be restricted based on population ratios compared to humans.

## 2.5 Rise
<!-- #### 2.5.1 Adventure  -->
<!-- **Abilities** -- Can use all weapons and armor, including enchanted or magical -->
<!-- items.\ -->
<!-- **Restrictions** -- Cannot learn or use spells.\ -->
<!-- **Starting Bonuses** -- Attack Bonus = +5%; Defense Target = 50%; Birthsign at 3 -->
<!-- IPs. -->
<!---->
<!-- #### 2.5.2 Order -->
<!-- **Abilities** -- Can learn and use Order spells.\ -->
<!-- **Restrictions** -- Spells, armor, and weapons restricted by Order.\ -->
<!-- **Starting Bonuses** -- Attack Bonus = +0%; Defense Target = 50%; Birthsign at 5 -->
<!-- IPs. -->
<!---->
<!-- #### 2.5.3 Magic -->
<!-- **Abilities** -- Can learn most spells and develop own spells.\ -->
<!-- **Restrictions** -- Cannot wear armor or use most weapons.\ -->
<!-- **Starting Bonuses** -- Attack Bonus = +0%; Defense Target = 55%; Birthsign at 6 -->
<!-- IPs. -->

## 2.6 Alignment
### 2.6.1 Relationship to Deities
<!-- TODO -->

### 2.6.2 Adherence to Ideals
<!-- TODO -->

### 2.6.3 Fealty
<!-- TODO -->

## 2.7 Languages
| Language             | Race/Alignment                                 |
|----------------------|------------------------------------------------|
| Common/Lingua Franca | Humans, commoners, most intelligent characters |
| Dwarven              | Dwarves                                        |
| Elven                | Elves                                          |
| Goblin/Orcish        | Goblins, orcs                                  |
| Halfing              | Halflings                                      |
| Hobgoblin            | Hobgoblins                                     |
| Kobold               | Kobolds                                        |
| Lizarding            | Lizardmen                                      |
| Ogrish               | Ogres, trolls                                  |

<!-- Table 1: Attribute scores and bonuses -->
<!---->
<!-- | Attribute Roll         | `01-05`      | `06-25`      | `26-75`      | `76-95`      | `96-00`      | -->
<!-- |------------------------|--------------|--------------|--------------|--------------|--------------| -->
<!-- | Score                  | Best         | Good         | Average      | Poor         | Bad          | -->
<!-- | STR                    |              |              |              |              |              | -->
<!-- | Attack Bonus           | +10%         | +5%          | 0%           | -5%          | -10%         | -->
<!-- | Carry Capacity         | 16#          | 15#          | 14#          | 12#          | 10#          | -->
<!-- | DEX                    |              |              |              |              |              | -->
<!-- | Reflex Saving Throw    | 50%          | 40%          | 30%          | 20%          | 15%          | -->
<!-- | Defense Bonus          | +10%         | +5%          | 0%           | -5%          | -10%         | -->
<!-- | END                    |              |              |              |              |              | -->
<!-- | Endurance Saving Throw | 50%          | 40%          | 30%          | 20%          | 15%          | -->
<!-- | Health Bonus           | +2           | +1           | 0            | -1           | -2           | -->
<!-- | INT                    |              |              |              |              |              | -->
<!-- | Starting IPs           | 2            | 1            | 1            | 1            | 0            | -->
<!-- | Languages Known        | 3 + noble**  | 2 + noble**  | 1 + noble**  | 1            | Illiterate*  | -->
<!-- | PER                    |              |              |              |              |              | -->
<!-- | Leadership Bonus       | +10%         | +5%          | 0%           | -5%          | -10%         | -->
<!-- | Conversation Bonus     | +10%         | +5%          | 0%           | -5%          | -10%         | -->
<!-- | WIS                    |              |              |              |              |              | -->
<!-- | Power Saving Throw     | 50%          | 40%          | 30%          | 20%          | 15%          | -->
<!-- | Sanity Bonus           | +2           | +1           | 0            | -1           | -2           | -->
<!-- | LKS                    |              |              |              |              |              | -->
<!-- | Reaction Bonus         | +10%         | +5%          | 0%           | -5%          | -10%         | -->
<!-- | Starting Wealth        | `7d6 x 10`sp | `6d6 x 10`sp | `5d6 x 10`sp | `4d6 x 10`sp | `3d6 x 10`sp | -->
<!-- | SOL                    |              |              |              |              |              | -->
<!-- | Mythic Saving Throw    | 30%          | 25%          | 20%          | 15%          | 10%          | -->
<!-- | Magic Effect Bonus     |              |              |              |              |              | -->
<!---->
<!-- \* -- Cannot read or write, but can speak (perhaps in a halting pace).\ -->
<!-- \*\* -- Noble means that the character can properly speak to high ranking -->
<!-- officials in that characters homeland/culture. -->
<!---->
<!-- Table 2: Backgrounds and descriptions -->
<!---->
<!-- | Background                  | Description                                  | Age Range | -->
<!-- |-----------------------------|----------------------------------------------|-----------| -->
<!-- | Agitator                    |                                              |           | -->
<!-- | Camp Follower               |                                              |           | -->
<!-- | Clerk                       |                                              |           | -->
<!-- | Con-man                     |                                              |           | -->
<!-- | Cottager or Serf            |                                              |           | -->
<!-- | Courtier                    |                                              |           | -->
<!-- | Craftsman                   |                                              |           | -->
<!-- | Entertainer                 |                                              |           | -->
<!-- | Farmer                      |                                              |           | -->
<!-- | Fugitive or Highwayman      |                                              |           | -->
<!-- | Holy man                    |                                              |           | -->
<!-- | Inquisitor                  |                                              |           | -->
<!-- | Lord or Lady                |                                              |           | -->
<!-- | Merchant                    |                                              |           | -->
<!-- | Mercenary                   |                                              |           | -->
<!-- | Ruffian                     |                                              |           | -->
<!-- | Sailor                      |                                              |           | -->
<!-- | Scholar                     |                                              |           | -->
<!-- | Smuggler                    |                                              |           | -->
<!-- | Soldier                     |                                              |           | -->
<!-- | Spy                         |                                              |           | -->
<!-- | Thief                       |                                              |           | -->
<!-- | Valet or Maid               |                                              |           | -->
<!-- | Village Elder or Wise Woman | *Minimum age must be at least 50 years old.* |           | -->
<!-- | Watchman                    |                                              |           | -->
<!-- | Wanderer                    |                                              |           | -->
<!-- | Woodsman                    |                                              |           | -->
<!---->
<!-- Table 3: Age range matrix -->
<!---->
<!-- Table 4: Background success matrix -->
<!---->
<!-- | Roll | `01-05` | `06-20` | `21-65` | `66-95` | `96-00` | -->
<!-- |------|---------|---------|---------|---------|---------| -->
<!-- |      |         |         |         |         |         | -->
<!---->
<!-- Table 5: Languages -->
<!---->

