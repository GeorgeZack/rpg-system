# 1 Introduction
## 1.1 D100
A `1d100` is used for % rolls, where success is counted when the `1d100` roll is
under or equal to the appropriate % being tested. Many dice sets now have two
`d10` die, with one numbering 0-9, and the other 00-90. The double digit dice is
used to determine the 10's value, while the single digit dice is used to
determine the 1's value. If that is not the case, then simply assign one dice to
the 10's value and the other to the 1's value. A roll of zero on the single
digit dice is considered 0 value except in one condition: A double zero (00, 0)
is considered a value of 100.

In the following examples, numbers in parentheses represent the number face up
on the physical dice, the value derived from the die roll follows after the
equal sign. Here are some example rolls:

- (60, 5) = `65`
- (10, 9) = `19`
- (90, 9) = `99`
- (50, 0) = `50`
- (00, 9) = `09`
- (00, 0) = `100`

## 1.2 Targets and Rolling Under
As previously mentioned, success is counted when the result is equal to or under
the % being tested against. A roll of 0 through 5 is always successful, and a
96 through 100 (where 100 is double 0's, the `1d100`'s equivalent to snake-eyes)
is always a failure unless the relevant target is over 100%, at which point only
100 is a failure.

Target percentages are determined or described by the referee, and players roll
"against" the target, looking to get a result under the target. Many times the
referee will ask for a particular attribute test, which can be translated to
the player looking at the character sheet, finding the attribute percent score,
and rolling against that number.

If a target is 100% or higher, there is still a chance to fail via rolling a
\100. 96 through 99 are no longer considered automatic failures. Though it is
unlikely, the test---if called for by the referee---should still be rolled
against. In combat, or in special situations where time is short, the 100%+
target can be "spread out" or split up to perform multiple actions in a round or
short time frame. Split the target in whichever manner the player desires
between the tests. The total sum of the split tests cannot exceed the original
percent, or rather all tests split up must add up to the original percent
target.

## 1.3 Types of Rolls
Most of the time, rolls will be determined by the referee to fall under an
attribute, the defense target of an enemy in combat, or a modified attribute.
Occasionally a test falls outside neat categories and therefore it is at the
liberty of the referee to determine either an attribute with a modifier, which
the modifier is determined on the spot, or a target percent which falls outside
the characters sheet. A simple "50%" is an example, and the player must roll
against that number rather than against anything on the character sheet.

### 1.3.1 Combat
Combat is quite simple as far as rolling tests are concerned. To determine a
successful attack the attacker must roll against the defenders defense target,
but adds (or subtracts) any modifiers such as attack bonus or specialty training
bonuses.

### 1.3.2 Unopposed Resolution
Unopposed Resolutions are used when a character may wish to do something that is
not automatically successful or guaranteed by the narrative. This roll includes
tests of strength in uncommon conditions or trying to accomplish a difficult
task in a short amount of time. Notably, it does not include competitive tests
or tasks where one character is directly opposing another.

### 1.3.3 Opposed Resolution
Opposed Resolutions occur when two or more characters are directly competing or
opposing each other in a specific task or conflict. This type of resolution is
utilized to determine the outcome of competitive interactions, such as contests,
negotiations, or any situation where the success of one character is directly
pitted against the success of another. The outcome is determined based on
comparative success or failure of each participant. Notably, this does not
necessarily include all combat rolls (such as attack rolls).

