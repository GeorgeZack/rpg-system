# 7 Improvement
Treasure abandonment = fewer IPs!

Experience help saving throws, combat prowess, last resort rolls, and calling
upon fate. Must be spent by the PC towards those increases though, so instead of
levels determining saving throw numbers, the PC spends IPs to increase them
individually.

More experience allows for special privileges, represented as prestige and
reputation. Calculated via X IPs spent and is used to essentially assist the
Referee in allowing PCs into certain clubs, talk to more important people,
accept better jobs, receive better rewards, and gain access to land for building
strongholds, wizards towers, guild-halls, etc.

Also special powers, but one must consult a great power to do so...

## 7.1 Spending
### 7.1.1 When
After acquiring IPs from dungeoneering and adventuring. Must be done at the end
or the beginning of a session, or in between sessions.

### 7.1.2 How
At a safe place, such as a town or stronghold, that has access to trainers and
experienced individuals who can teach characters how to improve abilities.

## 7.2 What to Improve
### 7.2.1 Saving Throws
To increase any attribute, spend **1 IP** to increase saving throw percentages
by `1d4`%.

### 7.2.2 Characteristics

- Hit Points -- **3 IPs** / **1 HP**, +1 IP req. per 4 HP
- Sanity Points -- **3 IPs** / **1 SP**, +1 IP req. per 4 SP
- Mana -- ...
- Attack Bonus -- **1 IP** / `1d4`%
- Defense Target -- **1 IP** / `1d4`%
- Call upon fate -- **3 IPs**

### 7.2.3 Training
To increase your chances of winning specialized tests, spend **2 IPs** to
increase that chance by `1d4`%.

Your natural ability to understand things, like survival or gambling, comes from
base stats. But future episodes involving such a skill can be enhanced by
experience. Improvement points are spent on the bonus so future episodes are
easier. A gambler that continues to practice will be skillful at it, but not
necessarily smarter overall.

### 7.2.4 Armor or Weapon Specialization
Voluntary choice with a bonus to attack and defense with particular chosen
weapon or armor, but all other usage suffers. <!-- TODO: Finish -->

## 7.3 Birthsigns
<!-- TODO -->
### 7.3.1 Banner
<!-- TODO -->
### 7.3.2 Candle
<!-- TODO -->
### 7.3.3 Fox
<!-- TODO -->
### 7.3.4 Fisher’s Net
<!-- TODO -->
### 7.3.5 Hands
<!-- TODO -->
### 7.3.6 Harp
<!-- TODO -->
### 7.3.7 Lady
<!-- TODO -->
### 7.3.8 Leaf
<!-- TODO -->
### 7.3.9 Lion
<!-- TODO -->
### 7.3.10 Ram
<!-- TODO -->
### 7.3.11 River
<!-- TODO -->
### 7.3.12 Shadow
<!-- TODO -->
### 7.3.13 Staff and Orb
<!-- TODO -->

## 7.4 Boons
Special powers that can be unlocked after meeting several requirements. These
are visible to players:

- Have enough IPs available
    - If other requirements aren't met, only 1 (?) IP is lost
- Have spent enough IPs required by Calling
- Perform a ceremony appropriate for the PCs religion or personal deity
    - Each deity requires an offering
    - Offerings include works prior to ceremony, items, HP or SP spent,
      or even sacrifices
