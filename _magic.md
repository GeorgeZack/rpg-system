# Magic

## vocab

### Spell
Anything that has a magical effect can be called a spell, this includes scrolls, enchantments, potions, classic spells (of course), summonings, anything that uses mana is considered a spell and will be referred to as such in this document. 

A theme may be decided by the ref will effect the required components for any spell, required components can add a default mana bonus, or simply be required for spells to be cast. For example, the theme may require that every spell uses a mage staff, if a staff is used, this may add a +4 mana bonus, and if a staff is not used, there may be a -10 mana subtraction. 

### Component
A Component consists of anything the magic user decides to add as an input, or focus for the spell. There are also required components that are used by refs to dictate the type of magic allowed in their world. A list of sample components can be fount [here](/spell_componenet.md)


## Mechanics of making a spell
The following steps are taken outside of combat, in a focused, meditative state. The ref may also require other things for spell development (A wizards tower, a cauldron, a sorcers stone, a crystal ball, meditation etc.)

1. Dictate the outcome of the spell in detail
    * The more detailed the outcome is, the easier it will be to choose the right components for a spell. 
    * Things thatare not described can have wild effects when the spell is cast in new enviromnments. Does that fireball catch items on fire, or does it only deal damage?
    * Similarly to how a wish needs to be very specific for things to not go awry, the spell outcome should be as clear as possible
    * For example "I turn invisable" is much less clear and specific than "I allow light to pass through me, and everyhing I am currently wearing, or holding. My shadow is not cast, but I can still interact with items in the world"
2. Make a list of [components](/spell_componenet.md), and explain what each component has to do with the outcome of the spell
    * Each Component can add or subtract a max of 5 points from the Component total. Each Component adds complexity, and cost to your spell, but add to the purity of your spell which improves the chance it casts. This number is assigned by the ref, not the player, so will need to be tested to see what works and what does not work. 
    * The ref will grade each Component, ensure that its clear what each Component contributes, so that its a bonus, and not a bane on your cast. The grade the ref chooses is not known to the player.
    * The ref assigns a score to each component based on factors such as creativity, rarity, magical connection, and relevance to the spell. The player does not assign these scores.
3. Decide a mana cost
    * Mana multiplied Component total is the number you need to roll under in order for a successful cast. A successful cast means that the spell outcome that you described takes full effect.
    * Max mana per spell = Willpower divided by 10
    * Mana may be finite, hard to replenish, or common. All of this is decided when a ref builds a world. Some examples of mana sources can be found [here](/mana.md).
    * Because this number is a multiplier, and willpower is soft capped at 100, this number will most often be between 1 and 10. 
4. Name, and log your spell into a spellbook, named spells are ready to cast.

As a ref, if a spell description is too incredible, add a negative mana cost, this ensures that spells with too powerful of a description are only castable to creatures that gain large amounts of willpower.

## Mechanics of casting a spell
1. Follow the steps, consume the Component listed, spend the mana all listed under the named spell you want to use.
3. If the mana cost, or Component total is negative, the spell fails, end casting. 
2. Roll a d100, if your dice roll is under the mana (known to you) multiplied by the Component cost (known only to the ref) then the defined spell outcome is achieved

## Example Spellbooks
### [Wizard Spellbook](/spellbooks/wizard_spellbook.md) Incantations, wand and staff magic, handwaving
### [Enchanters Spellbook]() Making items better with magic
### [Jew Wizard]() Making consumable magic items you can sell
### [Monster Spellbook]() Explaining the spells that monsters seem to use effortlessly