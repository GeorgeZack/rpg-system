-- ORIGINAL POST FROM DISCORD, PLEASE DO NOT USE AS IS. NOT ORIGINAL WORK --

Drugs and Addiction
- Drugs are a common means of suppressing horror and fear, or a tool used to
  even attempt to cure Madness by certain experimenting chemist-psychologists.
  Using drugs, alcohol, or other similar chemicals, one can temporarily suppress
  the horrors and fears lingering in their minds from the nightmare visions they
  have witnessed. The trouble is that they are, indeed, addicting and are likely
  to create a dependency. A botanist-chemist is most skilled in preparing doses
  of plant based drugs, a chemist in synthetic, or most anyone for simple
  alcohol. A medical doctor would be aware, as well, though by Hippocratic Oath
  obliged to not use them save for medical reasons. 

Natural Drugs & Liquor
- Alcohol is the cheapest and easiest to acquire - most any town or outpost
  sells liquor. Each dose of alcohol (varied by quality and potency) will
  provide a +1 to saves vs horror and fear for 1 hour. The downside is it causes
  a -1 to all other saves, and a natural 1 on a save vs poison will net you with
  alcoholism. 
- Ergot is a mold grown on wheat in cold, wet conditions. It is a
  hallucinogenic, providing a similar effect to alcohol for saves but also
  causing minor hallucinations. It is a neurotoxin, an overdose can kill you, so
  it should be prepared by a competent botanist. Smoked. A save vs poison is
  made at +4
- Mad Caps have the red and white coloration of deadly mushrooms, and while just
  as deadly normally the toxins can be leached with boiling water and then
  dried. They are extremely intoxicating and addictive, providing a +3 to saves
  vs fear and horror, a -3 to all other saves and rolls. The effects for a
  single dose lasts 3 hours and causes intense paranoia for 24 hours. A botanist
  is required to prepare it safely. Smoked. A save vs poison is made, flat.
- Red Tears are a variety of strained red algae suspended in alcohol. When
  ingested it produces mild hallucinations (which tend to be unpleasant) and a
  fairly potent anaesthetic effect. It’s not addictive, but is habit forming, so
  save vs poison with any wisdom bonuses applying.

Manufactured Drugs
- Morphine is a powerful anesthetic, extremely potent and extremely addictive.
  The effects of a single dose lasts 2 hours, giving a blissful feeling of
  serenity. +4 to saves vs fear and horror, no penalty to other rolls save
  initiative which suffers -4. A character is unable to feel pain, as well, and
  may push themselves past their physical limits. A save vs poison at -4 is
  required to avoid addiction. It is injected, and can only be prepared with a
  full pharmaceutical laboratory. A doctor or chemist is required to prepare it.
- Opium is a world-wide infamous drug, refined from poppies. It’s used, along
  with other drugs, in a number of medical tonics and elixirs. A dose lasts 2
  hours, making you drowsy. This gives you a -4 to initiative, and -2 to
  everything else. You have, meanwhile, +3 saves vs fear and horror. You roll a
  save vs poison at -2 to avoid addiction.
- Cocaine
- Laudanum

Magical Drugs
In addition to these organic and synthetic drugs, there are magical drugs
brought about by the Red Death’s desire to corrupt and ensnare.
- Black Lotus is a strange plant, similar to an opium poppy, native to Greece.
  The petals are as delicate as silk, as dark as a winter night. The lotus can
  be simply eaten, or brewed in tea. It is only mildly addictive, with a +1 to
  save vs addiction. The long-term effects of it’s use, however.. It grants a -2
  save vs fear and horror, but gives you the effects of Detect Magic, Detect
  Invisibility, and allows you to pierce illusions. A dose lasts an hour, and
  for every hour used you must make a Madness save. An alchemist or herbalist
  could prepare a viable sample - a botanist could also make an attempt, though
  it follows rules unfamiliar and strange to natural plants. 
- Jekyll's Brew is, truly, not the original serum. It is a more diluted form
  that bestows an extreme sensation of freedom, while overwhelming you with
  animalistic desires. Generally taken via injection, more modern varieties have
  been reduced to dry tablets that can be dissolved in liquid then imbibed.
  Liquids so infused emit a faint green luminescence. They are extremely
  addictive, a save vs poison at -4 is called on for a single use. The failure,
  at first, is merely addictive. Each successive use shifts the user’s alignment
  to chaotic evil, and on reaching CE the next failure turns them into a
  pathogenic lycanthrope most appropriate to their daily actions. A vile
  alchemist could, if they had the recipe and mystic ingredients, attempt to
  create doses of this hateful drug.
- Narseus is a magical drug concocted out of deep-sea plants and animal
  extracts. It grants an enormous bolstering of strength (1d8 points) and grants
  the ability to breathe underwater for 3 hours per dose. However, each dose is
  extremely addictive, calling for a -2 save vs addiction. It also causes the
  user to be able to hear the ‘song of the sea’ while near any significant
  source of water, calling for a Madness check.


