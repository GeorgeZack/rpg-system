# FAWCS
**F**antasy **A**dventure, **W**arfare, and **C**onquest **S**ystem

## Readme
Repository for rules.

## To do
- Determine rules missing needed for playtest
    - Melee and ranged combat modifiers, cover, visibility, prone, etc
    - Fumble and weapon breakage tables
    - Over 100% conditions, requirement (at start of round), and initiative
- 3.10 and 5.8 Retainers/hirelings
- Ref book -- Treasure generation/per monster type
- Ref book -- Basic monster list
- 3.12 Plot and housing costs, building costs, recommended costs for factions
  and quests
- 5.5.5 Poison and 5.6.3 drugs (see '_drugs.txt')
- 5.5 Incapacitation 1d6 minutes -> turns in combat, just make clear for ease of
  reading, double up/reference in chap 6
- Draft char sheet with cool wound chart
- 5.5.2 Ghost/death rules (must meet requirements), 5.10 spiritual realm
  minigame
- 5.6.2 Insanity recovery
- 3.5 Bandoleer, matchcord
- 1.3.3 and 1.3.4 -- Ref may set a % target arbitrarily depending on the
  situation, or may use [x] attribute with [y] formula...
- Translate Bennett's magic document
