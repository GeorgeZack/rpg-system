Players Booklet
1. Introduction
	1. D100
	2. Targets and Rolling Under
	3. Types of Rolls
		1. Combat
		2. Unopposed Resolution
		3. Opposed Resolution
2. Player Characters
	1. Procedure
	2. Attributes
		1. Strength
		2. Dexterity
		3. Endurance
		4. Looks
		5. Intelligence
		6. Wisdom
		7. Personality
		8. Soul
	3. Demi-Humans
		1. Rarity of Callings and Demi-Humans
	4. Backgrounds
		1. Time and Success
	5. Callings
		1. Adventure
		2. Order
		3. Magic
	6. Alignment
		1. Relationship to Deities
		2. Adherence to Ideals
		3. Fealty
	7. Languages
	8. Tables
3. Items and Services
	1. Encumbrance
	2. Coinage
	3. Armor
		1. Descriptions
		2. Magic Armor
	4. Weapons
		1. Melee Weapons
		2. Ranged Weapons
		3. Descriptions
		4. Initiative
		5. Silvered Weapons
		6. Reload
	5. General Equipment
		1. Descriptions
	6. Provisions
		1. Descriptions
		2. Filling Wineskins
	7. Specialist Equipment
		1. Descriptions
	8. Starting Equipment
		1. Descriptions
	9. Services
		1. Descriptions
	10. Hirelings
		1. Descriptions
	11. Transportation
		1. Descriptions
4. Spells
	1. Overview
		1. Mana
		2. Components
		3. Casting
	2. List
		1. Order
		2. Magic
5. Adventure
	1. The Party
	2. Time, Travel, Scale, and Rest
		1. Underworld
			1. Scale
			2. Time
			3. Travel
			4. Rest
			5. Light
			6. Mapping
			7. Traps
			8. Wandering Monsters
			9. Getting Lost
		2. Overworld
			1. Scale
			2. Time
			3. Travel
			4. Rest
			5. Weather
			6. Night
			7. Land
			8. Sea
			9. Air
			10. Getting Lost
	3. Fatigue
		1. Reducing Fatigue
	4. Saving Throws
		1. Reflex
		2. Endurance
		3. Power
		4. Mythic
	5. Health
		1. Healing
		2. Death
		3. Drugs
		4. Illness
		5. Poison
		6. Exposure
	6. Sanity
		1. Recovery
		2. Insanity
		3. Drugs
	7. Aging
	8. Hirelings
		1. Morale
		2. Monsters
		3. Treasure
	9. Fealty
		1. Score
		2. Changes in Score
		3. Role
		4. Duties
		5. Losing Fealty
		6. Changing Fealty
	10. The Spirit World
6. Combat
	1. Overview
		1. Distance
		2. Morale
		3. Combat Stamina
	2. Procedure
		1. Time
		2. Reaction
		3. Initiative
		4. Defense Target
		5. Attack Bonus
		6. Reloading
	3. Actions
		1. Major Actions
		2. Movement Actions
		3. Minor Actions
	4. Special Combat
		1. Grappling
			1. Actions
		2. Hand to Hand
			1. Actions
	5. Damage and Health
		1. Minor and Major Injuries
		2. Hitting Armor or Flesh
		3. Quick Chart
		4. Head
		5. Torso
		6. Arms
		7. Legs
		8. Fumbles
		9. Malfunctions
		10. Incapacitation and Death
	6. Theater of the Mind
		1. Distance
		2. Movement
7. Improvement
	1. Spending
		1. When
		2. How
	2. What to Improve
		1. Saving Throws
		2. Characteristics
		3. Training
		4. Armor and Weapon Specialization
	3. Birthsigns
		1. Banner
		2. Candle
		3. Fox
		4. Fisher’s Net
		5. Hands
		6. Harp
		7. Lady
		8. Leaf
		9. Lion
		10. Ram
		11. River
		12. Shadow
		13. Staff and Orb
	4. Boons

Referee Booklet
1. Introduction
2. Spells
	1. Creating a factory
	2. Using the factory
	3. Examples
3. Monsters
	1. Humanoid
	2. Animal
	3. Monstrous
	4. Undead

